Title: Livraisons
Order: 9
Date: 2018-06-02
Slug: livraisons
Authors: cgeek

> Cette page est un guide à destination des développeurs.

Duniter est livré sous la forme de différents binaires pour les environnements Linux et Windows.

La réalisation de ces livrables ainsi que leur mise à disposition est :

* Totalement automatisée pour :
  * Duniter Desktop (.deb et archive tar.gz)
  * Duniter Server (.deb)

* Partiellement automatisée pour :
  * Duniter Desktop Windows (.exe)
  * Duniter Server ARM (.deb)

## Livrables automatisés

Produire les livrables de Duniter Desktop et Duniter Server pour Linux (hors ARM) s'effectue depuis le code source de Duniter à l'aide du script :

    ./release/new_version.sh 1.6.24

Remplacer `1.6.24` par la version à produire.

Puis pousser le tag sur le dépôt GitLab :

    git push origin --tags

Dans les pipelines GitLab, une action manuelle de packaging sera disponible une fois les tests passés.

## Livrables semi-automatisés

Ces livrables possèdent un script pour produire le livrable, mais nécessitent un téléversement manuel sur le serveur GitLab pour être mis à disposition des utilisateurs.

### Duniter Server ARM

Suivre le tutoriel suivant : [https://duniter.org/en/wiki/duniter/create_arm_release/](https://duniter.org/en/wiki/duniter/create_arm_release/) (anglais)

### Duniter Desktop Windows

Produire ce livrable requiert les conditions suivantes :

* Disposer d'une machine Linux avec :
    * 4Go de RAM minimum
    * Bash
    * Node.js v6+
    * VirtualBox
    * Vagrant
    * Git
    * Yarn

Le script pour réaliser le build Windows est :

    ./release/scripts/build.sh make win 1.6.24

Remplacer `1.6.24` par la version à produire.

